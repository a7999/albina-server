rm $1/$2/$2_de.pdf
rm $1/$2/$2_it.pdf
rm $1/$2/$2_en.pdf
rm $1/$2/$2_fr.pdf
rm $1/$2/$2_es.pdf
rm $1/$2/$2_ca.pdf
rm $1/$2/$2_oc.pdf
rm $1/$2/$2_de_bw.pdf
rm $1/$2/$2_it_bw.pdf
rm $1/$2/$2_en_bw.pdf
rm $1/$2/$2_fr_bw.pdf
rm $1/$2/$2_es_bw.pdf
rm $1/$2/$2_ca_bw.pdf
rm $1/$2/$2_oc_bw.pdf
rm $1/$2/$2_AT-07_de.pdf
rm $1/$2/$2_AT-07_it.pdf
rm $1/$2/$2_AT-07_en.pdf
rm $1/$2/$2_AT-07_fr.pdf
rm $1/$2/$2_AT-07_es.pdf
rm $1/$2/$2_AT-07_ca.pdf
rm $1/$2/$2_AT-07_oc.pdf
rm $1/$2/$2_AT-07_de_bw.pdf
rm $1/$2/$2_AT-07_it_bw.pdf
rm $1/$2/$2_AT-07_en_bw.pdf
rm $1/$2/$2_AT-07_fr_bw.pdf
rm $1/$2/$2_AT-07_es_bw.pdf
rm $1/$2/$2_AT-07_ca_bw.pdf
rm $1/$2/$2_AT-07_oc_bw.pdf
rm $1/$2/$2_IT-32-BZ_de.pdf
rm $1/$2/$2_IT-32-BZ_it.pdf
rm $1/$2/$2_IT-32-BZ_en.pdf
rm $1/$2/$2_IT-32-BZ_fr.pdf
rm $1/$2/$2_IT-32-BZ_es.pdf
rm $1/$2/$2_IT-32-BZ_ca.pdf
rm $1/$2/$2_IT-32-BZ_oc.pdf
rm $1/$2/$2_IT-32-BZ_de_bw.pdf
rm $1/$2/$2_IT-32-BZ_it_bw.pdf
rm $1/$2/$2_IT-32-BZ_en_bw.pdf
rm $1/$2/$2_IT-32-BZ_fr_bw.pdf
rm $1/$2/$2_IT-32-BZ_es_bw.pdf
rm $1/$2/$2_IT-32-BZ_ca_bw.pdf
rm $1/$2/$2_IT-32-BZ_oc_bw.pdf
rm $1/$2/$2_IT-32-TN_de.pdf
rm $1/$2/$2_IT-32-TN_it.pdf
rm $1/$2/$2_IT-32-TN_en.pdf
rm $1/$2/$2_IT-32-TN_fr.pdf
rm $1/$2/$2_IT-32-TN_es.pdf
rm $1/$2/$2_IT-32-TN_ca.pdf
rm $1/$2/$2_IT-32-TN_oc.pdf
rm $1/$2/$2_IT-32-TN_de_bw.pdf
rm $1/$2/$2_IT-32-TN_it_bw.pdf
rm $1/$2/$2_IT-32-TN_en_bw.pdf
rm $1/$2/$2_IT-32-TN_fr_bw.pdf
rm $1/$2/$2_IT-32-TN_es_bw.pdf
rm $1/$2/$2_IT-32-TN_ca_bw.pdf
rm $1/$2/$2_IT-32-TN_oc_bw.pdf
rm $1/$2/$2_ES-CT-L_de.pdf
rm $1/$2/$2_ES-CT-L_it.pdf
rm $1/$2/$2_ES-CT-L_en.pdf
rm $1/$2/$2_ES-CT-L_fr.pdf
rm $1/$2/$2_ES-CT-L_es.pdf
rm $1/$2/$2_ES-CT-L_ca.pdf
rm $1/$2/$2_ES-CT-L_oc.pdf
rm $1/$2/$2_ES-CT-L_de_bw.pdf
rm $1/$2/$2_ES-CT-L_it_bw.pdf
rm $1/$2/$2_ES-CT-L_en_bw.pdf
rm $1/$2/$2_ES-CT-L_fr_bw.pdf
rm $1/$2/$2_ES-CT-L_es_bw.pdf
rm $1/$2/$2_ES-CT-L_ca_bw.pdf
rm $1/$2/$2_ES-CT-L_oc_bw.pdf

cp $1/$2/$3/*.pdf $1/$2/
cp $1/$2/$3/$2_de.pdf $1/$2/
cp $1/$2/$3/$2_it.pdf $1/$2/
cp $1/$2/$3/$2_en.pdf $1/$2/
cp $1/$2/$3/$2_fr.pdf $1/$2/
cp $1/$2/$3/$2_es.pdf $1/$2/
cp $1/$2/$3/$2_ca.pdf $1/$2/
cp $1/$2/$3/$2_oc.pdf $1/$2/
cp $1/$2/$3/$2_de_bw.pdf $1/$2/
cp $1/$2/$3/$2_it_bw.pdf $1/$2/
cp $1/$2/$3/$2_en_bw.pdf $1/$2/
cp $1/$2/$3/$2_fr_bw.pdf $1/$2/
cp $1/$2/$3/$2_es_bw.pdf $1/$2/
cp $1/$2/$3/$2_ca_bw.pdf $1/$2/
cp $1/$2/$3/$2_oc_bw.pdf $1/$2/
cp $1/$2/$3/$2_AT-07_de.pdf $1/$2/
cp $1/$2/$3/$2_AT-07_it.pdf $1/$2/
cp $1/$2/$3/$2_AT-07_en.pdf $1/$2/
cp $1/$2/$3/$2_AT-07_fr.pdf $1/$2/
cp $1/$2/$3/$2_AT-07_es.pdf $1/$2/
cp $1/$2/$3/$2_AT-07_ca.pdf $1/$2/
cp $1/$2/$3/$2_AT-07_oc.pdf $1/$2/
cp $1/$2/$3/$2_AT-07_de_bw.pdf $1/$2/
cp $1/$2/$3/$2_AT-07_it_bw.pdf $1/$2/
cp $1/$2/$3/$2_AT-07_en_bw.pdf $1/$2/
cp $1/$2/$3/$2_AT-07_fr_bw.pdf $1/$2/
cp $1/$2/$3/$2_AT-07_es_bw.pdf $1/$2/
cp $1/$2/$3/$2_AT-07_ca_bw.pdf $1/$2/
cp $1/$2/$3/$2_AT-07_oc_bw.pdf $1/$2/
cp $1/$2/$3/$2_IT-32-BZ_de.pdf $1/$2/
cp $1/$2/$3/$2_IT-32-BZ_it.pdf $1/$2/
cp $1/$2/$3/$2_IT-32-BZ_en.pdf $1/$2/
cp $1/$2/$3/$2_IT-32-BZ_fr.pdf $1/$2/
cp $1/$2/$3/$2_IT-32-BZ_es.pdf $1/$2/
cp $1/$2/$3/$2_IT-32-BZ_ca.pdf $1/$2/
cp $1/$2/$3/$2_IT-32-BZ_oc.pdf $1/$2/
cp $1/$2/$3/$2_IT-32-BZ_de_bw.pdf $1/$2/
cp $1/$2/$3/$2_IT-32-BZ_it_bw.pdf $1/$2/
cp $1/$2/$3/$2_IT-32-BZ_en_bw.pdf $1/$2/
cp $1/$2/$3/$2_IT-32-BZ_fr_bw.pdf $1/$2/
cp $1/$2/$3/$2_IT-32-BZ_es_bw.pdf $1/$2/
cp $1/$2/$3/$2_IT-32-BZ_ca_bw.pdf $1/$2/
cp $1/$2/$3/$2_IT-32-BZ_oc_bw.pdf $1/$2/
cp $1/$2/$3/$2_IT-32-TN_de.pdf $1/$2/
cp $1/$2/$3/$2_IT-32-TN_it.pdf $1/$2/
cp $1/$2/$3/$2_IT-32-TN_en.pdf $1/$2/
cp $1/$2/$3/$2_IT-32-TN_fr.pdf $1/$2/
cp $1/$2/$3/$2_IT-32-TN_es.pdf $1/$2/
cp $1/$2/$3/$2_IT-32-TN_ca.pdf $1/$2/
cp $1/$2/$3/$2_IT-32-TN_oc.pdf $1/$2/
cp $1/$2/$3/$2_IT-32-TN_de_bw.pdf $1/$2/
cp $1/$2/$3/$2_IT-32-TN_it_bw.pdf $1/$2/
cp $1/$2/$3/$2_IT-32-TN_en_bw.pdf $1/$2/
cp $1/$2/$3/$2_IT-32-TN_fr_bw.pdf $1/$2/
cp $1/$2/$3/$2_IT-32-TN_es_bw.pdf $1/$2/
cp $1/$2/$3/$2_IT-32-TN_ca_bw.pdf $1/$2/
cp $1/$2/$3/$2_IT-32-TN_oc_bw.pdf $1/$2/
cp $1/$2/$3/$2_ES-CT-L_de.pdf $1/$2/
cp $1/$2/$3/$2_ES-CT-L_it.pdf $1/$2/
cp $1/$2/$3/$2_ES-CT-L_en.pdf $1/$2/
cp $1/$2/$3/$2_ES-CT-L_fr.pdf $1/$2/
cp $1/$2/$3/$2_ES-CT-L_es.pdf $1/$2/
cp $1/$2/$3/$2_ES-CT-L_ca.pdf $1/$2/
cp $1/$2/$3/$2_ES-CT-L_oc.pdf $1/$2/
cp $1/$2/$3/$2_ES-CT-L_de_bw.pdf $1/$2/
cp $1/$2/$3/$2_ES-CT-L_it_bw.pdf $1/$2/
cp $1/$2/$3/$2_ES-CT-L_en_bw.pdf $1/$2/
cp $1/$2/$3/$2_ES-CT-L_fr_bw.pdf $1/$2/
cp $1/$2/$3/$2_ES-CT-L_es_bw.pdf $1/$2/
cp $1/$2/$3/$2_ES-CT-L_ca_bw.pdf $1/$2/
cp $1/$2/$3/$2_ES-CT-L_oc_bw.pdf $1/$2/

chmod 755 $1/$2/*.pdf

